const navConfig = [
  {
    subheader: 'Quản lý sản phẩm',
    items: [
      {
        title: 'Loại sản phẩm',
        path: '/categories',
      },
      {
        title: 'Kiểu sản phẩm',
        path: '/product-types',
      },
      {
        title: 'Màu',
        path: '/colors',
      },
      {
        title: 'Sản phẩm',
        path: '/products',
      },
    ],
  },
  {
    subheader: 'Khác',
    items: [
      {
        title: 'Ảnh bìa',
        path: '/covers',
      },
      {
        title: 'Nhân viên hỗ trợ',
        path: '/supporters',
      },
      {
        title: 'Người đăng ký',
        path: '/subscribers',
      },
      {
        title: 'Liên hệ',
        path: '/contacts',
      },
    ],
  },
];

export default navConfig;
